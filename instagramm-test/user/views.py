from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet
from user.models import Favourites
from user.models import User
from user.permissions import IsUserOwnerOrReadOnly
from user.serializers import UserSerializer
from rest_framework import status
from rest_framework.response import Response
from publication.models import Publications
from rest_framework.views import APIView

class UserView(ModelViewSet):
    queryset = User.objects.prefetch_related('post_owner')
    serializer_class = UserSerializer
    lookup_field = 'pk'
    permission_classes = (IsUserOwnerOrReadOnly, )


class PostFavouriteView(APIView):

    def get(self, request, pk):
        user = User.objects.get(id=pk)
        favourite = Favourites.objects.values_list('publication__text', flat=True).filter(user=user)
        return Response(favourite)


class FavouriteView(APIView):

    def get(self, request, pk):
        user = request.user
        publication = Publications.objects.get(id=pk)
        if Favourites.objects.filter(user=user, publication=publication).exists():
            Favourites.objects.filter(user=user, publication=publication).delete()
            return Response('Favourite Deleted', status=status.HTTP_201_CREATED)
        else:
            Favourites.objects.create(user=user, publication=publication)
            return Response('Favourite Created', status=status.HTTP_200_OK)